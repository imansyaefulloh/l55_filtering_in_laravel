@if(array_intersect(array_keys(request()->query()), array_keys($mappings)))
    <p>
        <a href="{{ route('courses.index') }}" class="btn btn-danger" style="width: 100%;">&times; Clear all filters</a>
    </p>
@endif

@foreach($mappings as $key => $map)
    @include('courses.partials._filter_list', [
        'map' => $map,
        'key' => $key
    ])
@endforeach